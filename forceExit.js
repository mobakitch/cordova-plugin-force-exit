var exec = cordova.require('cordova/exec');

exports.exit = function(success, fail) {
    exec(success, fail, 'ForceExit', 'exit', []);
};
