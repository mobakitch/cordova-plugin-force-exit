package org.apache.cordova;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Build;
import android.util.Log;

public class ForceExit extends CordovaPlugin {

    private static final String TAG = "ForceExit";
    private static final int FORCE_EXIT_COUNT = 5;
    private static int _exitCount = 0;

    @Override
    public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        if ("exit".equals(action)) {
            String strVer = "OS Version " + Build.VERSION.SDK_INT;
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                // KitKatはOpenGLの不具合でApplicationの再利用を
                // 何回か行うと表示ができなくなるため、強制終了する.
                _exitCount++;
                if (_exitCount > FORCE_EXIT_COUNT) {
                    // 5回目で強制終了
                    System.exit(0);
                } else {
                    cordova.getActivity().finish();
                }
            } else {
                // その他のOS Verは普通に終了
                cordova.getActivity().finish();
            }
            return true;
        }
        return false;
    }
}
